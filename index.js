// Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

var mongoose = require('mongoose');
mongoose.connect("mongodb://localhost:27017/CRUD_Pizza365", function (error) {
    if (error) throw error;
    console.log('Successfully connected');
});

// Khai báo thư viện path
const path = require("path");
const drinkModel = require("./app/models/drinkModel");
const voucherModel = require("./app/models/voucherModel");
const orderModel = require("./app/models/orderModel");
const userModel = require("./app/models/userModel");
const drinkRouter = require("./app/routers/drinkRouter");
const voucherRouter = require("./app/routers/voucherRouter");
const userRouter = require("./app/routers/userRouter");
const orderRouter = require("./app/routers/orderRouter");
// Khởi tạo app express
const app = express();

app.use(express.json());

app.use(express.urlencoded({
    urlencoded: true,
}));

// Khai báo cổng của project
const port = 8000;

// Khai báo sử dụng tài nguyên
app.use(express.static("views"));

// Khai báo API dạng get "/" sẽ chạy vào đây
// Callback function: Là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/pizza365index.html"));
})

app.get("/viewOrder.html", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/viewOrder.html"));
})

app.get("/pizza365_nhanvien.html", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/pizza365_nhanvien.html.html"));
})

app.get("/orderdetail.html", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/orderdetail.html.html"));
})

// import middeware
app.use("/", drinkRouter);
app.use("/", voucherRouter);
app.use("/", userRouter);
app.use("/", orderRouter);

// Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
})
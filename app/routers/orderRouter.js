const express = require("express");
const orderMiddleware = require("../middlewares/orderMiddeware");
const orderRouter = express.Router();

orderRouter.use(orderMiddleware);

// import các hàm module controller
const { getAllOrder, getOrderById, createOrder, updateOrderById, deleteOrderById } = require("../controllers/orderController");

orderRouter.get("/orders", getAllOrder);

orderRouter.get("/orders/:orderId", getOrderById);

orderRouter.put("/orders/:orderId", updateOrderById);

orderRouter.post("/orders", createOrder);

orderRouter.delete("/orders/:orderId", deleteOrderById);

module.exports = orderRouter;
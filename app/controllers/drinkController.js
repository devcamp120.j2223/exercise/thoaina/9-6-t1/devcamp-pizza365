// import course model
const { mongoose } = require("mongoose");
const drinkModel = require("../models/drinkModel");

// get all drinks
const getAllDrink = (req, res) => {
    drinkModel.find((error, data) => {
        if (error) {
            res.status(500).json({
                message: `Internal server error: ${error.message}`,
            });
        }
        else {
            res.status(200).json({
                ...data
            });
        }
    })
};

const getDrinkById = (req, res) => {
    let id = req.params.drinkId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        drinkModel.findById(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

// tạo mới drink
const createDrink = (req, res) => {
    // B1: thu thập dữ liệu
    let body = req.body;

    // B2: Kiểm tra dữ liệu
    if (!body.maNuocUong) {
        res.status(400).json({
            message: "maNuocUong is require!",
        })
    }
    else if (!body.tenNuocUong) {
        res.status(400).json({
            message: "tenNuocUong is require!",
        })
    }
    else if (!Number.isInteger(body.donGia) || body.donGia < 0) {
        res.status(400).json({
            message: "donGia is invalid!",
        })
    }
    else {
        // B3: Thực hiện thao tác nghiệp vụ
        let drink = {
            _drinkId: mongoose.Types.ObjectId(),
            maNuocUong: body.maNuocUong,
            tenNuocUong: body.tenNuocUong,
            donGia: body.donGia
        };
        drinkModel.create(drink, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(201).json({
                    data
                });
            }
        });
    }
};

const updateDrinkById = (req, res) => {
    let id = req.params.drinkId;
    let body = req.body;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else if (!body.maNuocUong) {
        res.status(400).json({
            message: "maNuocUong is require!",
        })
    }
    else if (!body.tenNuocUong) {
        res.status(400).json({
            message: "tenNuocUong is require!",
        })
    }
    else if (!Number.isInteger(body.donGia) || body.donGia < 0) {
        res.status(400).json({
            message: "donGia is invalid!",
        })
    }
    else {
        let drink = {
            maNuocUong: body.maNuocUong,
            tenNuocUong: body.tenNuocUong,
            donGia: body.donGia
        };
        drinkModel.findByIdAndUpdate(id, drink, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const deleteDrinkById = (req, res) => {
    let id = req.params.drinkId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        drinkModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(204).json({
                    data
                });
            }
        })
    }
};
// export các hàm
module.exports = { getAllDrink, getDrinkById, createDrink, updateDrinkById, deleteDrinkById };
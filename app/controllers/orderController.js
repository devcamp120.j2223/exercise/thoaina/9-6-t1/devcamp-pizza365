// import order model
const { mongoose } = require("mongoose");
const orderModel = require("../models/orderModel");

// get all orders
const getAllOrder = (req, res) => {
    orderModel.find((error, data) => {
        if (error) {
            res.status(500).json({
                message: `Internal server error: ${error.message}`,
            });
        }
        else {
            res.status(200).json({
                ...data
            });
        }
    })
};

const getOrderById = (req, res) => {
    let id = req.params.orderId;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        orderModel.findById(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

// tạo mới order
const createOrder = (req, res) => {
    // B1: thu thập dữ liệu
    let body = req.body;

    // B2: Kiểm tra dữ liệu
    if (!body.orderCode) {
        res.status(400).json({
            message: "orderCode is require!",
        })
    }
    else if (!body.pizzaSize) {
        res.status(400).json({
            message: "pizzaSize is require!",
        })
    }
    else if (!body.pizzaType) {
        res.status(400).json({
            message: "pizzaType is require!",
        })
    }
    else if (!body.status) {
        res.status(400).json({
            message: "status is require!",
        })
    }
    else {
        // B3: Thực hiện thao tác nghiệp vụ
        let order = {
            _orderId: mongoose.Types.ObjectId(),
            orderCode: body.orderCode,
            pizzaSize: body.pizzaSize,
            pizzaType: body.pizzaType,
            status: body.status
        };
        orderModel.create(order, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(201).json({
                    data
                });
            }
        });
    }
};

const updateOrderById = (req, res) => {
    let id = req.params.orderId;
    let body = req.body;

    if (!body.orderCode) {
        res.status(400).json({
            message: "orderCode is require!",
        })
    }
    else if (!body.pizzaSize) {
        res.status(400).json({
            message: "pizzaSize is require!",
        })
    }
    else if (!body.pizzaType) {
        res.status(400).json({
            message: "pizzaType is require!",
        })
    }
    else if (!body.status) {
        res.status(400).json({
            message: "status is require!",
        })
    }
    else {
        let order = {
            orderCode: body.orderCode,
            pizzaSize: body.pizzaSize,
            pizzaType: body.pizzaType,
            status: body.status
        };
        orderModel.findByIdAndUpdate(id, order, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(200).json({
                    data
                });
            }
        })
    }
};

const deleteOrderById = (req, res) => {
    let id = req.params.orderId;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        res.status(400).json({
            message: "id is invalid!",
        })
    }
    else {
        orderModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                res.status(500).json({
                    message: `Internal server error: ${error.message}`,
                });
            }
            else {
                res.status(204).json({
                    data
                });
            }
        })
    }
};
// export các hàm
module.exports = { getAllOrder, getOrderById, createOrder, updateOrderById, deleteOrderById };

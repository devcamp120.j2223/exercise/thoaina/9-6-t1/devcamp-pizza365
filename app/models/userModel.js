// Khai báo
const mongoose = require("mongoose");

// Khai báo thư viện Schema
const Schema = mongoose.Schema;

// Tạo đối tượng Schema tương ứng với Collection
const userSchema = new Schema({
    _userId: {
        type: mongoose.Types.ObjectId,
        unique: true
    },
    fullName: {
        type: String,
        require: true,
    },
    email: {
        type: String,
        require: true,
        unique: true,
    },
    address: {
        type: String,
        require: true,
    },
    phone: {
        type: String,
        require: true,
        unique: true,
    },
    order: [
        {
            type: mongoose.Types.ObjectId,
            ref: "Order"
        }
    ],
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    },
});

// export 
module.exports = mongoose.model("User", userSchema);
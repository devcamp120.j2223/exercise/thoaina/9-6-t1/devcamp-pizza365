// Khai báo
const mongoose = require("mongoose");

// Khai báo thư viện Schema
const Schema = mongoose.Schema;

// Tạo đối tượng Schema tương ứng với Collection
const drinkSchema = new Schema({
    _drinkId: mongoose.Types.ObjectId,
    maNuocUong: {
        type: String,
        unique: true,
    },
    tenNuocUong: {
        type: String,
        require: true,
        unique: true,
    },
    donGia: {
        type: Number,
        required: true,
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    },
});

// export 
module.exports = mongoose.model("Drink", drinkSchema);